from setuptools import setup

setup(
    name='gmod-math',
    version='',
    packages=['gmod_math', 'gmod_math.gmod_core', 'gmod_math.gmod_geom', 'gmod_math.gmod_trig',
              'gmod_math.liber_primus', 'gmod_math.gmod_numtheory'],
    url='',
    license='',
    author='Shawn Shroyer',
    author_email='',
    description=''
)
