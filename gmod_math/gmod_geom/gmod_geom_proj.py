from .gmod_geom_aff3 import *


def j_func(l1, m1, n1, l2, m2, n2):
    x = m1 * n2 - m2 * n1
    y = n1 * l2 - n2 * l1
    z = l2 * m1 - m2 * l1
    return GmodHPoint(x, y, z)


def J_func(x1, y1, z1, x2, y2, z2):
    l = y1 * z2 - y2 * z1
    m = z1 * x2 - z2 * x1
    n = x2 * y1 - y2 * x1
    return GmodHLine(l, m, n)


class GmodHPlottable:

    def __plot_all_ah__(self, ahplot, style):
        self.__plot_on_3d__(ahplot.ax_3d.ax, style)
        self.__plot_on_xy__(ahplot.ax_xy, style)
        self.__plot_on_yz__(ahplot.ax_yz, style)
        self.__plot_on_xz__(ahplot.ax_xz, style)

    def __plot_on_3d__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_3d__")

    def __plot_on_xy__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_xy__")

    def __plot_on_yz__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_yz__")

    def __plot_on_xz__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_xz__")


class GmodHPoint(GmodProportionStruct, GmodHPlottable):

    def __init__(self, x, y, z):
        assert not (x == y == z == 0)
        super().__init__(x, y, z)

    @property
    def x(self):
        return Gmod(self[0])

    @property
    def y(self):
        return Gmod(self[1])

    @property
    def z(self):
        return Gmod(self[2])

    @property
    def dual_line(self):
        return GmodHLine(self.x, self.y, self.z)

    @property
    def is_null_point(self):
        return self.is_incident_with_line(self.dual_line)

    @property
    def null_point_prop(self):
        assert self.is_null_point
        for prop in GmodProportionStruct.ALL_UNIQUE_PROPORTIONS:
            if GmodHPoint.null_point_for_prop(prop) == self:
                return prop

        return False

    @property
    def incident_lines(self):
        """
        Find point's incident lines
        :return: (list) list of points line passes through
        """
        lines = list()
        for line in GmodHLine.ALL_UNIQUE_LINES:
            if self.is_incident_with_line(line):
                lines.append(line)

        return lines

    def __eq__(self, other):
        assert isinstance(other, GmodHPoint)
        return super().__eq__(other)

    def quadr_between_point(self, other):
        assert isinstance(other, GmodHPoint)
        x1 = self.x
        y1 = self.y
        z1 = self.z
        x2 = other.x
        y2 = other.y
        z2 = other.z
        return Gmod(1) - (x1 * x2 + y1 * y2 - z1 * z2) ** 2 // (
                (x1 ** 2 + y1 ** 2 - z1 ** 2) * (x2 ** 2 + y2 ** 2 - z2 ** 2))

    def is_dual_of_line(self, line):
        assert isinstance(line, GmodHLine)
        return self.dual_line() == line

    def is_incident_with_line(self, line):
        assert isinstance(line, GmodHLine)
        return line.l * self.x + line.m * self.y - line.n * self.z == Gmod(0)

    def join_point(self, other):
        assert isinstance(other, GmodHPoint)
        if self.is_null_point() and other.is_null_point():
            join_l = self.join_null_points(other)
            assert join_l.is_incident_with_point(self) and join_l.is_incident_with_point(other)
            return join_l
        else:
            join_l = J_func(self.x, self.y, self.z, other.x, other.y, other.z)
            assert join_l.is_incident_with_point(self) and join_l.is_incident_with_point(other)
            return join_l

    def is_colinear_with_points(self, other1, other2):
        assert isinstance(other1, GmodHPoint) and isinstance(other2, GmodHPoint)
        return self.join_point(other1).is_incident_with_point(other2)

    def is_perp_to_point(self, other):
        assert isinstance(other, GmodHPoint)
        return Gmod((self.corr_mat() @ other.corr_mat()).trace()) == 0

    @property
    def corr_mat(self):
        return GmodMatrix([[self.y, self.x + self.z],
                           [self.x - self.z, -self.y]])

    def is_midpoint_of_bc(self, b, c):
        if not self.is_null_point():
            return GmodHTransform(self).refl_b_in_a(b) == c.corr_mat
        else:
            raise NotImplementedError("Unknown midpoint of null point")

    def join_null_points(self, other):
        assert self.is_null_point() and other.is_null_point()
        prop1 = self.null_point_prop()
        prop2 = other.null_point_prop()
        t1 = prop1[0]
        u1 = prop1[1]
        t2 = prop2[0]
        u2 = prop2[1]
        l = u1 * u2 - t1 * t2
        m = t1 * u2 + t2 * u1
        n = u1 * u2 + t1 * t2
        return GmodHLine(l, m, n)

    def make_a2_point(self, index=2):
        if self[index] == 0:
            return GmodA2Point.ORIGIN_2D
        scaled = self.reduce_to_index(index)
        if index == 2:
            return GmodA2Point(scaled[0], scaled[1])
        elif index == 0:
            return GmodA2Point(scaled[1], scaled[2])
        elif index == 1:
            return GmodA2Point(scaled[0], scaled[2])
        else:
            raise ValueError("Invalid index " + str(index))

    def __plot_on_xy__(self, ax, style):
        ax.plot(self.make_a2_point(2), style)

    def __plot_on_yz__(self, ax, style):
        ax.plot(self.make_a2_point(0), style)

    def __plot_on_xz__(self, ax, style):
        ax.plot(self.make_a2_point(1), style)

    def __plot_on_3d__(self, ax, style):
        x = [int(self.x)]
        y = [int(self.y)]
        z = [int(self.z)]

        if style:
            ax.plot(x, y, z, style)
        else:
            ax.plot(x, y, z)

    @staticmethod
    def null_point_for_prop(prop):
        t = Gmod(prop[0])
        u = Gmod(prop[1])

        x = u ** 2 - t ** 2
        y = Gmod(2) * u * t
        z = u ** 2 + t ** 2
        return GmodHPoint(x, y, z)


class GmodHLine(GmodProportionStruct, GmodHPlottable):

    def __init__(self, l, m, n):
        super().__init__([l, m, n])
        assert not (l == m == n == 0)

    @property
    def l(self):
        return Gmod(self[0])

    @property
    def m(self):
        return Gmod(self[1])

    @property
    def n(self):
        return Gmod(self[2])

    def __str__(self) -> str:
        result = "("
        result += str(self.l) + ", "
        result += str(self.m) + ", "
        result += str(self.n)
        result += ")"
        if self.is_null_line:
            result += " (null)"
        return result

    @property
    def dual_point(self):
        return GmodHPoint(self.l, self.m, self.n)

    @property
    def incident_points(self):
        """
        Iterate through all unique points and find which the line is
        incident with
        :return: (list) list of points line passes through
        """
        points = list()
        for point in GmodHPoint.ALL_UNIQUE_POINTS:
            if self.is_incident_with_point(point):
                points.append(point)

        return points

    @property
    def is_null_line(self):
        return self.is_incident_with_point(self.dual_point)

    @property
    def null_line_proportion(self):
        """
        Gets the unique proportion for the null line
        The returned proportion can be passed to null_line_for_prop for the
        same null line
        Returns:
             (GmodProportion) of null line
        """
        assert self.is_null_line()
        for prop in GmodProportionStruct.ALL_UNIQUE_PROPORTIONS:
            if GmodHLine.null_line_for_prop(prop) == self:
                return prop
        return False

    @property
    def null_circle_intersects(self):
        int_points = list()
        for null_point in GmodHPoint.ALL_NULL_POINTS:
            if self.is_incident_with_point(null_point):
                int_points.append(null_point)
        return int_points

    def __eq__(self, other) -> bool:
        assert isinstance(other, GmodHLine)
        return super().__eq__(other)

    def join_line(self, other):
        assert isinstance(other, GmodHLine)
        if self.is_null_line() and other.is_null_line():
            join_p = self.join_null_lines(other)
            assert self.is_incident_with_point(join_p) and other.is_incident_with_point(join_p)
            return join_p
        else:
            join_p = j_func(self.l, self.m, self.n, other.l, other.m, other.n)
            assert self.is_incident_with_point(join_p) and other.is_incident_with_point(join_p)
            return join_p

    def is_dual_of_point(self, point):
        assert isinstance(point, GmodHPoint)
        return self.dual_point() == point

    def is_incident_with_point(self, point):
        return self.l * point.x + self.m * point.y - self.n * point.z == 0

    def is_perp_to_line(self, other):
        assert isinstance(other, GmodHLine)
        return self.is_incident_with_point(other.dual_point()) or other.is_incident_with_point(self.dual_point())

    def is_concurrent_with(self, other1, other2):
        assert isinstance(other1, GmodHLine) and isinstance(other2, GmodHLine)
        return other2.is_incident_with_point(self.join_line(other1))

    def altitude_line(self, point):
        assert isinstance(point, GmodHPoint)
        return self.dual_point().join_point(point)

    def spread(self, other):
        l1 = self.l
        m1 = self.m
        n1 = self.n
        l2 = other.l
        m2 = other.m
        n2 = other.n
        return Gmod(1) - (l1 * l2 + m1 * m2 - n1 * n2) ** 2 // (
                (l1 ** 2 + m1 ** 2 - n1 ** 2) * (l2 ** 2 + m2 ** 2 - n2 ** 2))

    def join_null_lines(self, other):
        """
        Join two null lines
        Parameters:
             other (GmodHLine) -- join of null lines
        """
        assert self.is_null_line() and other.is_null_line()
        prop1 = self.null_line_proportion
        prop2 = other.null_line_proportion
        t1 = prop1[0]
        u1 = prop1[1]
        t2 = prop2[0]
        u2 = prop2[1]
        x = u1 * u2 - t1 * t2
        y = t1 * u2 + t2 * u1
        z = u1 * u2 + t1 * t2
        return GmodHPoint(x, y, z)

    def make_a2_line(self, index=2):
        if self[index] == 0:
            return GmodA2Line(0, 0, 0)
        scaled = self.reduce_to_index(index)
        if index == 2:
            return GmodA2Line(scaled[0], scaled[1], 0)
        elif index == 0:
            return GmodA2Line(scaled[1], scaled[2], 0)
        elif index == 1:
            return GmodA2Line(scaled[0], scaled[2], 0)
        else:
            raise ValueError("Invalid index " + str(index))

    def __plot_on_3d__(self, ax, style):
        for point in self.incident_points:
            point.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        ax.plot(self.make_a2_line(2), style)

    def __plot_on_yz__(self, ax, style):
        ax.plot(self.make_a2_line(0), style)

    def __plot_on_xz__(self, ax, style):
        ax.plot(self.make_a2_line(1), style)

    @staticmethod
    def colinear_line_for_points(point1, point2, point3):
        assert isinstance(point1, GmodHPoint) and \
               isinstance(point2, GmodHPoint) and \
               isinstance(point3, GmodHPoint)

        assert point1.is_colinear_with_points(point2, point3)
        coline = point1.join_point(point2)
        assert coline.is_incident_with_point(point1) and \
               coline.is_incident_with_point(point2) and \
               coline.is_incident_with_point(point3)

        return coline

    @staticmethod
    def null_line_for_prop(prop):
        """
        Calculates the null line for the proportion. Each unique proportion
        should have a unique null point
        Paramaters:
            prop (GmodProportion) -- proportion to find null line of
        Returns:
             (GmodHLine) null line for proportion
        """
        t = Gmod(prop[0])
        u = Gmod(prop[1])
        l = u ** 2 - t ** 2
        m = Gmod(2) * u * t
        n = u ** 2 + t ** 2
        return GmodHLine(l, m, n)


GmodHPoint.ALL_UNIQUE_POINTS = [GmodHPoint(*prop) for prop in GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS]

GmodHLine.X_AXIS = GmodHLine(0, 1, 0)
GmodHLine.Y_AXIS = GmodHLine(1, 0, 0)
GmodHLine.LINE_AT_INFINITY = GmodHLine(0, 0, 1)
GmodHLine.ALL_UNIQUE_LINES = [point.dual_line for point in GmodHPoint.ALL_UNIQUE_POINTS]


class GmodHCouple:

    def __init__(self, point, line):
        # assert self.line1 != self.line2
        self.a = point
        self.L = line

        self.is_dual = self.a.is_dual_with(self.L)
        self.altitude_line = self.a.join_point(self.L.dual_point())
        self.altitude_point = self.a.dual_line().join_line(self.L)

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["a"] = str(self.a)
        self_dict["L"] = str(self.L)
        return self_dict


class GmodHSide(GmodHPlottable):
    """
    A side with points a1, a2
    """

    def __init__(self, point1, point2):
        """
        Creates a side with a1 and a2
        :param point1: (GmodHPoint) point1
        :param point2: (GmodHPoint) point2
        """
        assert not (point1 is None) and not (point2 is None)

        assert isinstance(point1, GmodHPoint) and isinstance(point2, GmodHPoint)
        # assert self.point1 != self.point2
        self.a1 = point1
        self.a2 = point2

    @property
    def side_points(self):
        return [self.a1, self.a2]

    @property
    def side_line(self):
        return self.a1.join_point(self.a2)

    @property
    def midpoint(self):
        return (self.a1 + self.a2) * Gmod(1 / 2)

    @property
    def quadr(self):
        """
        Calculates the quadrance between the two points of the side
        :return: (Gmod) the quadrance of the two points
        """
        return self.a1.quadr(self.a2)

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["a1"] = str(self.a1)
        self_dict["a2"] = str(self.a2)
        self_dict["q"] = str(self.quadr)
        self_dict["side_line"] = str(self.side_line)
        return self_dict

    def __plot_on_xy__(self, ax, style):
        aff1 = self.a1.make_affine_point(2)
        aff2 = self.a2.make_affine_point(2)
        ax.plot(GmodA2Side(aff1, aff2), style)

    def __plot_on_yz__(self, ax, style):
        aff1 = self.a1.make_affine_point(0)
        aff2 = self.a2.make_affine_point(0)
        ax.plot(GmodA2Side(aff1, aff2), style)

    def __plot_on_xz__(self, ax, style):
        aff1 = self.a1.make_affine_point(1)
        aff2 = self.a2.make_affine_point(1)
        ax.plot(GmodA2Side(aff1, aff2), style)

    def __plot_on_3d__(self, ax, style):
        xs = [int(self.a1.x), int(self.a2.x)]
        ys = [int(self.a1.y), int(self.a2.y)]
        zs = [int(self.a1.z), int(self.a2.z)]

        if style:
            style = "-" + style
            ax.plot(xs, ys, zs, style)
        else:
            ax.plot(xs, ys, zs)
        # ax.annotate('(%s, %s, %s)' % xyz, xyz=xyz, textcoords='data')


class GmodHVertex:

    def __init__(self, line1, line2):
        # assert self.line1 != self.line2
        self.L1 = line1
        self.L2 = line2
        self.vertex_point = self.L1.join_line(self.L2)
        self.spread = self.L1.spread(self.L2)

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["vertex_point"] = str(self.vertex_point)
        self_dict["L1"] = str(self.L1)
        self_dict["L2"] = str(self.L2)
        self_dict["S"] = str(self.spread)

        return self_dict


class GmodHVector(GmodHPlottable):

    def __init__(self, x, y, z):
        self.x = Gmod(x)
        self.y = Gmod(y)
        self.z = Gmod(z)

    @property
    def vect_mat(self):
        return GmodMatrix([self.x, self.y, self.z])

    @property
    def quadr(self):
        return self.x ** 2 + self.y ** 2 + self.z ** 2

    def __eq__(self, other):
        assert isinstance(other, GmodHVector)
        return self.vect_mat == other.vect_mat

    def __neg__(self):
        neg_mat = -self.vect_mat
        return GmodHVector(neg_mat[0], neg_mat[1], neg_mat[2])

    def __add__(self, other):
        assert isinstance(other, GmodHVector)
        add_mat = self.vect_mat + other.vect_mat
        return GmodHVector(add_mat[0], add_mat[1], add_mat[2])

    def __sub__(self, other):
        assert isinstance(other, GmodHVector)
        sub_mat = self.vect_mat - other.vect_mat
        return GmodHVector(sub_mat[0], sub_mat[1], sub_mat[2])

    def __mul__(self, other):
        other = Gmod(other)
        scale_mat = self.vect_mat * other
        return GmodHVector(scale_mat[0], scale_mat[1], scale_mat[2])

    def is_touching_point(self, point):
        assert isinstance(point, GmodHPoint)
        if self.x == point.x and self.y == point.y:
            return True
        else:
            return False

    def is_perp_to_vect(self, other):
        assert isinstance(other, GmodHVector)
        return self.quadr - other.quadr == (other - self).quadr

    def is_para_to_vect(self, other):
        assert isinstance(other, GmodHVector)
        x1 = self.x
        y1 = self.y
        z1 = self.z
        x2 = other.x
        y2 = other.y
        z2 = other.z
        return x1 * y2 - y1 * x2 == y1 * z2 - z1 * y2 == z1 * x2 - x1 * z2 == 0

    def dot(self, other):
        assert isinstance(other, GmodHVector)
        return self.x * other.x + self.y * other.y + self.z * other.z

    def spread(self, other):
        assert isinstance(other, GmodHVector)
        return Gmod(1) - (self.dot(other) ** 2) // (self.quadr * other.quadr)

    def volume_of_parapiped(self, other1, other2):
        assert isinstance(other1, GmodHVector) and isinstance(other2, GmodHVector)
        return GmodMatrix([self.vect_mat, other1.vect_mat, other2.vect_mat]).det()

    def solid_spread(self, other1, other2):
        assert isinstance(other1, GmodHVector) and isinstance(other2, GmodHVector)
        return (self.volume_of_parapiped(other1, other2) ** 2) // (self.quadr * other1.quadr * other2.quadr)

    def projective_quadr(self, other):
        assert isinstance(other, GmodHVector)
        mat_V = GmodMatrix([[self.dot(self), self.dot(other)], [self.dot(other), other.dot(other)]])
        return mat_V.det() // (self.quadr * other.quadr)

    def __plot_on_3d__(self, ax, style):
        GmodHPoint(self.x, self.y, self.z).__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        GmodHPoint(self.x, self.y, self.z).__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        GmodHPoint(self.x, self.y, self.z).__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        GmodHPoint(self.x, self.y, self.z).__plot_on_xz__(ax, style)

    def __strrepr__(self):
        return json.dumps(self.__printdict__, indent=8)

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["vec_coords"] = self.vect_mat.__strrepr__()
        self_dict["quadr"] = str(self.quadr)
        return self_dict


class GmodHNgon(GmodHPlottable):

    def __init__(self, *args):
        assert len(args) >= 3

        self.a_n = list()
        for arg in args:
            assert isinstance(arg, GmodHPoint)
            self.a_n.append(arg)

    @property
    def __printdict__(self):
        print_dict = dict()
        print_dict["vertices"] = dict()
        for x in range(len(self.ngon_verts)):
            print_dict["vertices"]["vert" + str(x)] = self.ngon_verts[x].__printdict__
        print_dict["sides"] = dict()
        for x in range(len(self.ngon_sides)):
            print_dict["sides"]["side" + str(x)] = self.ngon_sides[x].__printdict__
        return print_dict

    @property
    def ngon_sides(self):
        length = len(self.a_n)
        ngon_sides = list()
        for x in range(length):
            curr_a = self.a_n[x % length]
            next_a = self.a_n[(x + 1) % length]
            curr_side = GmodHSide(curr_a, next_a)
            ngon_sides.append(curr_side)
        return ngon_sides

    @property
    def ngon_quadrs(self):
        return [side.quadr for side in self.ngon_sides]

    @property
    def ngon_lines(self):
        return [side.side_line for side in self.ngon_sides]

    @property
    def ngon_verts(self):
        length = len(self.ngon_lines)
        ngon_verts = list()
        for x in range(length):
            curr_L = self.ngon_lines[x % length]
            next_L = self.ngon_lines[(x + 1) % length]
            curr_vert = GmodHVertex(curr_L, next_L)
            ngon_verts.append(curr_vert)
        return ngon_verts

    @property
    def ngon_spreads(self):
        return [vert.spread for vert in self.ngon_verts]

    @property
    def ngon_crosses(self):
        return [Gmod(1) - spread for spread in self.ngon_spreads]

    def is_touching_point(self, point):
        assert isinstance(point, GmodHPoint)
        for side in self.ngon_sides:
            if side.side_line.is_incident_with_point(point):
                return True

        return False

    def __plot_on_3d__(self, ax, style):
        for n_point in self.a_n:
            n_point.__plot_on_3d__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for n_point in self.a_n:
            n_point.__plot_on_xy__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        for n_point in self.a_n:
            n_point.__plot_on_yz__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        for n_point in self.a_n:
            n_point.__plot_on_xz__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_xz__(ax, style)


class GmodHTriangle(GmodHNgon, GmodHPlottable):

    def __init__(self, point1, point2, point3):
        assert isinstance(point1, GmodHPoint) and \
               isinstance(point2, GmodHPoint) and \
               isinstance(point3, GmodHPoint)

        assert not point1.is_colinear_with_points(point2, point3)
        super().__init__(point1, point2, point3)

    @property
    def is_dual(self):
        return self.a_n[0].is_dual_of_line(self.ngon_lines[1]) or \
               self.a_n[1].is_dual_of_line(self.ngon_lines[2]) or \
               self.a_n[2].is_dual_of_line(self.ngon_lines[0])

    @property
    def triangle_altitudes(self):
        alt1 = GmodHCouple(self.a_n[0], self.ngon_lines[1])
        alt2 = GmodHCouple(self.a_n[1], self.ngon_lines[2])
        alt3 = GmodHCouple(self.a_n[2], self.ngon_lines[0])
        return [alt1, alt2, alt3]

    @property
    def triangle_orthocenters(self):
        ortho1 = self.triangle_altitudes[0].altitude_line.join_line(self.triangle_altitudes[1].altitude_line)
        ortho2 = self.triangle_altitudes[1].altitude_line.join_line(self.triangle_altitudes[2].altitude_line)
        ortho3 = self.triangle_altitudes[2].altitude_line.join_line(self.triangle_altitudes[0].altitude_line)
        return [ortho1, ortho2, ortho3]

    @property
    def triangle_ortholines(self):
        ortho1 = self.triangle_altitudes[0].altitude_point.join_point(self.triangle_altitudes[1].altitude_point)
        ortho2 = self.triangle_altitudes[1].altitude_point.join_point(self.triangle_altitudes[2].altitude_point)
        ortho3 = self.triangle_altitudes[2].altitude_point.join_point(self.triangle_altitudes[0].altitude_point)
        return [ortho1, ortho2, ortho3]

    @property
    def pyth_theorem_perp(self):
        q1 = self.ngon_quadrs[0],
        q2 = self.ngon_quadrs[1]
        q3 = self.ngon_quadrs[2]
        return q3 == q1 + q2 - q1 * q2

    @property
    def triple_quad_colinear(self):
        q1 = self.ngon_quadrs[0]
        q2 = self.ngon_quadrs[1]
        q3 = self.ngon_quadrs[2]
        return (q1 + q2 + q3) ** 2 == Gmod(2) * (q1 ** 2 + q2 ** 2 + q3 ** 2) + Gmod(4) * q1 * q2 * q3

    @property
    def cross_law(self):
        q1 = self.ngon_quadrs[0]
        q2 = self.ngon_quadrs[1]
        q3 = self.ngon_quadrs[2]
        S1 = self.ngon_spreads[0]
        S2 = self.ngon_spreads[1]
        S3 = self.ngon_spreads[2]

        return (q1 * q2 * S3 - q1 - q2 - q3 + Gmod(2)) ** 2 == Gmod(4) * (Gmod(1) - q1) * (Gmod(1) - q2) * (
                Gmod(1) - q3)

    @property
    def spread_law(self):
        q1 = self.ngon_quadrs[0]
        q2 = self.ngon_quadrs[1]
        q3 = self.ngon_quadrs[2]
        S1 = self.ngon_spreads[0]
        S2 = self.ngon_spreads[1]
        S3 = self.ngon_spreads[2]

        return S1 // q1 == S2 // q2 == S3 // q3

    @property
    def __printdict__(self):
        tri_dict = super().__printdict__
        tri_dict["laws"] = dict()
        tri_dict["laws"]["TripleQuadColinear"] = str(self.triple_quad_colinear)
        tri_dict["laws"]["PythTheoremPerp"] = str(self.pyth_theorem_perp)
        tri_dict["laws"]["SpreadLaw"] = str(self.spread_law)
        tri_dict["laws"]["CrossLaw"] = str(self.cross_law)

        return tri_dict

    def __plot_on_3d__(self, ax, style):
        pass
        for side in self.ngon_sides:
            side.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for side in self.ngon_sides:
            side.__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        for side in self.ngon_sides:
            side.__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        for side in self.ngon_sides:
            side.__plot_on_xz__(ax, style)


class GmodHTrilateral(GmodHTriangle):

    def __init__(self, line1, line2, line3):
        assert isinstance(line1, GmodHLine) and \
               isinstance(line2, GmodHLine) and \
               isinstance(line3, GmodHLine)
        assert not line1.concurrent_with(line2, line3)
        self.L1 = line1
        self.L2 = line2
        self.L3 = line3

        a1 = line1.join_line(line3)
        a2 = line2.join_line(line3)
        a3 = line1.join_line(line2)

        super().__init__(a1, a2, a3)

    @property
    def __printdict__(self):
        self_dict = super().__printdict__
        self_dict["L1"] = str(self.L1)
        self_dict["L2"] = str(self.L2)
        self_dict["L3"] = str(self.L3)
        return self_dict


class GmodHQuad(GmodHNgon, GmodHPlottable):
    """
        denotes a rectangle in the ffe with:
            point of bottom left of rect ["bot-l"]
            point of top right of rect ["top-r"]

       can accept GmodPoints
    """

    def __init__(self, a1, a2, a3, a4):
        super().__init__(a1, a2, a3, a4)

    @property
    def quad_diags(self):
        diag_ac = GmodA2Side(self.a_n[0], self.a_n[2])
        diag_bd = GmodA2Side(self.a_n[1], self.a_n[3])
        return [diag_ac, diag_bd]

    @property
    def __printdict__(self):
        quad_dict = super().__printdict__

        for x in range(len(self.quad_diags)):
            quad_dict["diag" + str(x)] = self.quad_diags[x].side_info

        quad_dict["center"] = self.quad_center

        quad_dict["laws"] = dict()
        quad_dict["laws"]["IsParallelogram"] = str(self.is_parallelogram)
        quad_dict["laws"]["IsRhombus"] = str(self.is_rhombus)
        return quad_dict

    @property
    def is_parallelogram(self):
        return self.ngon_lines[0].is_perp_to_line(self.ngon_lines[2]) and \
               self.ngon_lines[1].is_perp_to_line(self.ngon_lines[3])

    @property
    def is_rhombus(self):
        return self.is_parallelogram() and \
               self.quad_diags[0].side_line.is_perp_with(self.quad_diags[1].side_line)

    @property
    def quad_center(self):
        assert self.is_parallelogram()
        return self.quad_diags[0].side_line.intersection_point(self.quad_diags[1].side_line)

    def get_midpoint_quad(self):
        """ page 49 (share the same center) """
        return GmodA2Quad(self.ngon_sides[0].midpoint,
                          self.ngon_sides[1].midpoint,
                          self.ngon_sides[2].midpoint,
                          self.ngon_sides[3].midpoint)


class GmodHCircle(GmodHPlottable):

    def __init__(self, center, radius):
        assert isinstance(center, GmodHPoint)
        self.c = center
        self.k = Gmod(radius)

    def point_on_circle(self, point):
        return self.c.quadr_between_point(point) == self.k

    def __printdict__(self):
        self_dict = dict()
        self_dict["center"] = self.c
        self_dict["radius"] = self.k
        return self_dict

    @staticmethod
    def intersects_with_unit_circle(prop):
        assert isinstance(prop, (GmodHPoint, GmodHLine))
        unit_intersects = list()
        if isinstance(prop, GmodHPoint):
            for null_line in GmodHLine.ALL_NULL_LINES:
                if null_line.is_incident_with_point(prop):
                    unit_intersects.append(null_line)

        if isinstance(prop, GmodHLine):
            for null_point in GmodHPoint.ALL_NULL_POINTS:
                if null_point.is_incident_with_line(prop):
                    unit_intersects.append(null_point)

        return unit_intersects

    @property
    def all_circle_points(self):
        intersects = list()
        for point in GmodHPoint.ALL_UNIQUE_POINTS:
            if point.quadr(self.c) == self.k:
                intersects.append(point)
        return intersects

    def __plot_on_3d__(self, ax, style):
        for point in self.all_circle_points:
            point.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for point in self.all_circle_points:
            aff = point.make_affine_point(2)
            ax.plot(aff, style)

    def __plot_on_yz__(self, ax, style):
        for point in self.all_circle_points:
            aff = point.make_affine_point(0)
            ax.plot(aff, style)

    def __plot_on_xz__(self, ax, style):
        for point in self.all_circle_points:
            aff = point.make_affine_point(1)
            ax.plot(aff, style)


class GmodHSphere:

    def __init__(self, center, radius):
        self.center = center
        self.radius = Gmod(radius)

    def point_on_circle(self, point):
        return self.center.quadr(point) == self.radius

    def __printdict__(self):
        self_dict = dict()
        self_dict["center"] = self.center
        self_dict["radius"] = self.radius
        return self_dict


class GmodHTransform:

    def __init__(self, *args):
        """

        :param args:GmodHPoint,GmodHLine
        """
        if len(args) == 1:
            trans = args[0]
            if isinstance(trans, GmodHPoint):
                self.a = trans
                self.type = "point-transform"
            elif isinstance(trans, GmodHLine):
                self.L = trans
                self.type = "line-transform"

    def refl_b_in_a(self, b):
        assert hasattr(self, "a_point") and isinstance(b, GmodHPoint)
        # Null reflection theorem
        if self.a.is_null_point():
            return self.a.corr_mat

        a_mat = self.a.corr_mat
        b_mat = b.corr_mat
        return a_mat @ b_mat @ a_mat

    # @staticmethod
    # def point_for_corr_mat(corr_mat):


class GmodHPlot:

    @staticmethod
    def add_3d_gridlines(ax, xlabel, ylabel, zlabel):
        ax.set_xlim([0, 29])
        ax.set_ylim([0, 29])
        ax.set_zlim([0, 29])
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)

        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()

        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='red')
        # Customize the minor grid
        ax.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
        # Turn off the display of all ticks.
        # ax.tick_params(which='both',  # Options for both major and minor ticks
        #              top=False,  # turn off top ticks
        #             left=False,  # turn off left ticks
        #            right=False,  # turn off right ticks
        #           bottom=False)  # turn off bottom ticks
        ax.view_init(azim=-115, elev=17)

    def __init__(self, ax, xlabel="x", ylabel="y", zlabel="z"):
        self.ax = ax
        self.plotted_geoms = list()

        GmodHPlot.add_3d_gridlines(self.ax, xlabel, ylabel, zlabel)

    def plot(self, geom_object, style=False):
        if not style:
            style = "ro"

        if isinstance(geom_object, GmodHPlottable):
            geom_object.__plot_on_3d__(self, style)
            self.plotted_geoms.append(geom_object)
        elif isinstance(geom_object, list):
            for geom in geom_object:
                self.plot(geom, style)
        else:
            assert isinstance(geom_object, GmodHPlottable)

    def __str__(self) -> str:
        result = "***************\n\n"
        for geom in self.plotted_geoms:
            result += str(geom)
            result += "\n\n***************"
        return result

    def __repr__(self):
        return self.__str__()


ALL_NULL_POINTS = GmodMatrix([GmodHPoint.null_point_for_prop(prop)
                              for prop in GmodProportionStruct.ALL_UNIQUE_PROPORTIONS], row_type=GmodHPoint)
ALL_NULL_LINES = GmodMatrix([GmodHLine.null_line_for_prop(prop)
                             for prop in GmodProportionStruct.ALL_UNIQUE_PROPORTIONS], row_type=GmodHLine)

"""

setup for Unit and Projective circle parameterization 
note that e(infinity) = [-1, 0]    

"""
"""
GmodHPoint.UNIQUE_PROPORTIONS = [[Gmod(1), gmod] for gmod in Gmod.ALL_GMODS]
GmodHPoint.UNIT_CIRCLE_PARAM = [GmodHCircle.unit_circ_param(gmod) for gmod in Gmod.ALL_GMODS]
GmodHPoint.PROJ_CIRCLE_PARAM = [GmodHCircle.proj_circ_param(gmod) for gmod in Gmod.ALL_GMODS]
GmodHPoint.ALL_NULL_POINTS = [GmodHCircle.null_point_for_proportion(prop) for prop in GmodHPoint.UNIQUE_PROPORTIONS]
GmodHLine.ALL_NULL_LINES = [GmodHCircle.null_line_for_proportion(prop) for prop in GmodHPoint.UNIQUE_PROPORTIONS]


# start of tests
print()
ST1_a = GmodHPoint(-3,3,5)
ST1_b = GmodHPoint(4,0,5)
ST1_c = GmodHPoint(2,-4,5)
ST1 = GmodHTriangle(ST1_a,ST1_b,ST1_c)

ST2_a = GmodHPoint(-1,0,1)
ST2_b = GmodHPoint(3,4,5)
ST2_c = GmodHPoint(-7,-24,25)
ST2 = GmodHTriangle(ST2_a, ST2_b, ST2_c)

for gmod in Gmod.ALL_GMODS:
    print("Gmod: " + str(gmod))
    print("Prop: " + str(GmodHPoint.UNIQUE_PROPORTIONS[int(gmod)]))
    print("Unit Circle Rat: " + str(GmodHPoint.UNIT_CIRCLE_PARAM[int(gmod)]))
    print("Proj Circle Rat: " + str(GmodHPoint.PROJ_CIRCLE_PARAM[int(gmod)]))
    print("Null Point: " + str(GmodHPoint.ALL_NULL_POINTS[int(gmod)]))
    print("Null Line: " + str(GmodHLine.ALL_NULL_LINES[int(gmod)]))
    print()
    print()
"""
