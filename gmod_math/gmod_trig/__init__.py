import gmod_geom
from gmod_core import *

MASTER_CIS_COMPLEX = GmodMatrix([Gmod(row[0], row[1]) for row in gmod_geom.UNIT_CIRCLE.to_gmod_matrix])
# print(MASTER_CIS_COMPLEX)

MASTER_CIS_ISPACE = GmodMatrix([[x, x.ispace12, x.ispace17] for x in MASTER_CIS_COMPLEX])


# print(MASTER_CIS_ISPACE)


def euler_for_gmod(gmod):
    for comp in MASTER_CIS_COMPLEX:
        for scale in Gmod.ALL_GMODS:
            if scale * gmod == comp:
                return comp


def master_cis_eval(gmod):
    gmod = Gmod(gmod)
    if gmod == 0:
        return GmodMatrix([0, 0, 0])

    cis12 = [row[0] for row in MASTER_CIS_ISPACE if row[1] == gmod]
    cis17 = [row[0] for row in MASTER_CIS_ISPACE if row[2] == gmod]
    return [gmod, *cis12, *cis17]


MASTER_CIS = GmodMatrix([master_cis_eval(gmod) for gmod in Gmod.ALL_GMODS])


def cis(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_eval = MASTER_CIS[some_gmod, :]
    if ispace is None:
        return GmodMatrix(cis_eval[1], cis_eval[2])
    elif ispace == 12:
        return GmodMatrix(cis_eval[1])
    elif ispace == 17:
        return GmodMatrix(cis_eval[2])
    else:
        return None


def cos(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_to_cos = (lambda some_cis: some_cis.greal)
    return cis(some_gmod, ispace).apply(cis_to_cos)


def sin(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_to_sin = (lambda some_cis: some_cis.gimag)
    return cis(some_gmod, ispace).apply(cis_to_sin)


def tan(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_to_tan = (lambda some_cis: some_cis.greal / some_cis.gimag)
    return cis(some_gmod, ispace).apply(cis_to_tan)


def arccos(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_to_arccos = (lambda some_cis: 1 / some_cis.greal)
    return cis(some_gmod, ispace).apply(cis_to_arccos)


def arcsin(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_to_arcsin = (lambda some_cis: 1 / some_cis.gimag)
    return cis(some_gmod, ispace).apply(cis_to_arcsin)


def arctan(some_gmod, ispace=None):
    some_gmod = Gmod(some_gmod)
    cis_to_arctan = (lambda some_cis: some_cis.gimag / some_cis.greal)
    return cis(some_gmod, ispace).apply(cis_to_arctan)


class TrigDict(dict):

    def __init__(self, some_gmod, ispace=None):
        super().__init__(cis=cis(some_gmod, ispace),
                         cos=cos(some_gmod, ispace),
                         sin=sin(some_gmod, ispace),
                         tan=tan(some_gmod, ispace),
                         arccos=arccos(some_gmod, ispace),
                         arcsin=arcsin(some_gmod, ispace),
                         arctan=arctan(some_gmod, ispace))

    @property
    def eval_matrix(self):
        return GmodMatrix([self["cis"],
                           self["cos"],
                           self["sin"],
                           self["tan"],
                           self["arccos"],
                           self["arcsin"],
                           self["arctan"]])


def calculate_e():
    all_eulers = [Gmod(point[0], point[1]) for point in gmod_geom.to_gmod_matrix]
    print(all_eulers)
    for possible_e in Gmod.ALL_GMODS:
        matches_cis = True
        for gmod in Gmod.ALL_GMODS:
            euler_left = possible_e ** Gmod(0, gmod)
            print(possible_e, gmod, euler_left)
            if matches_cis:
                if euler_left not in all_eulers:
                    matches_cis = False
        if matches_cis:
            print(possible_e, matches_cis)
            for gmod in Gmod.ALL_GMODS:
                print(possible_e ** Gmod(0, gmod))


MASTER_TRIG_12 = GmodMatrix([TrigDict(gmod, 12).eval_matrix for gmod in Gmod.ALL_GMODS])
MASTER_TRIG_17 = GmodMatrix([TrigDict(gmod, 17).eval_matrix for gmod in Gmod.ALL_GMODS])

MASTER_TRIG = GmodMatrix([TrigDict(gmod).eval_matrix for gmod in Gmod.ALL_GMODS])

# calculate_e()
