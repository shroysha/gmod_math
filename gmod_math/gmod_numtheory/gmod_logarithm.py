from gmod_core import *


def revlog(value, base):
    value = Gmod(value)
    base = Gmod(base)
    revlogs = list()
    for testbase in Gmod.ALL_GMODS:
        if value ** testbase == base:
            revlogs.append(testbase)

    return revlogs


def logbase(value, base):
    value = Gmod(value)
    base = Gmod(base)
    basepows = list()
    for testbase in Gmod.ALL_GMODS:
        if testbase ** value == base:
            basepows.append(testbase)

    return basepows
