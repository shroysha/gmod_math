from gmod_core import *

sqrt5 = Gmod(11), Gmod(18)
sqrt5_1 = sqrt5[0]
sqrt5_2 = sqrt5[1]


def is_in_golden_ratio(a, b):
    a = Gmod(a)
    b = Gmod(b)
    return (a + b) // a == a // b


def is_in_conj_golden_ratio(a, b):
    a = Gmod(a)
    b = Gmod(b)
    return (a + b) // a == b // a


phi = Gmod(6), Gmod(24)
phi1 = phi[0]
phi2 = phi[1]


def is_phi_value(maybe_phi):
    """
    evaluates if a number is solution to x^2 - x - 1 == 0
    the answer will be equiv to
    (1 + sqrt(5)) // 2
    the golden ratio and inverse golden ratio have a set
    of symmetries that preserve and interlate them
    they are preserved by fractional linear transformations
    x, 1 / 1-x , x-1 / x
    they are reciprocals symmetric about 1/2 and projectively
    around 2
    :param maybe_phi:
    :return:
    """
    return maybe_phi ** 2 == maybe_phi + Gmod(1)


Phi = Gmod(5), Gmod(23)
Phi1 = Phi[0]
Phi2 = Phi[1]


def is_Phi_value(maybe_Phi):
    """
    check if value is a possible value for conjugate phi
    equivalent to
    1-sqrt(5) // 2
    :param maybe_Phi:
    :return:
    """
    return maybe_Phi ** 2 == - maybe_Phi + Gmod(1)


psi = Gmod(10)


def is_psi_value(maybe_psi):
    """
    check if value is an answer to the equation
    0 = x**3 - x**2 - 1
    :param maybe_psi:
    :return:
    """
    return maybe_psi ** 3 == maybe_psi ** 2 + Gmod(1)


def gr_preserve_linear_trans(value):
    value = Gmod(value)

    map1 = value
    map2 = Gmod(1) // (Gmod(1) - value)
    map3 = (value - Gmod(1)) // value
    return GmodMatrix(map1, map2, map3)


def gr_interchange_linear_trans(value):
    value = Gmod(value)

    map1 = Gmod(1) // value
    map2 = Gmod(1) - value
    map3 = value // (value - Gmod(1))
    return GmodMatrix(map2, map3, map1)


def find_phi_a_eval(value, ratio):
    for gmod in Gmod.ALL_GMODS:
        if (value + gmod) // value == value // gmod == ratio:
            return gmod


def find_phi_b_eval(value, ratio):
    for gmod in Gmod.ALL_GMODS:
        if (gmod + value) // gmod == gmod // value == ratio:
            return gmod


def phi_trans_map(value) -> GmodMatrix:
    preserve = gr_preserve_linear_trans(value)
    interchange = gr_interchange_linear_trans(value)

    return GmodMatrix([[*preserve], [*interchange]])


class GRSet:

    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.ratio = a // b

    def __str__(self) -> str:
        self_str = "GRSet:"
        self_str += "a:" + str(self.a) + ","
        self_str += "b:" + str(self.b) + ","
        self_str += "ratio:" + str(self.ratio)
        return self_str

    __repr__ = __str__

#
# def phi_mats_containing(value):
#     if value == 0:
#         return GoldenRatio.PHI_MATS[0]
#     containing_value = list()
#     for phi_set in GoldenRatio.PHI_MATS:
#         if value in phi_set and value != phi_set[0]:
#             containing_value.append(phi_set)
#
#     return containing_value
#
#
# def trans_mats_containing(value):
#     if value == 0:
#         return GoldenRatio.PHI_TRANS_MATS[0]
#
#     containing_value = list()
#     for phi_set in GoldenRatio.PHI_TRANS_MATS:
#         if value in phi_set and value != phi_set[0]:
#             containing_value.append(phi_set)
#
#     return containing_value
# assert GoldenRatio.is_phi_value(GoldenRatio.phi1)
# assert GoldenRatio.is_phi_value(GoldenRatio.phi2)
# assert GoldenRatio.is_Phi_value(GoldenRatio.Phi1)
# assert GoldenRatio.is_Phi_value(GoldenRatio.Phi2)
# assert GoldenRatio.is_psi_value(GoldenRatio.psi)
#
# assert GoldenRatio.phi1 == (GoldenRatio.sqrt5_1 + Gmod(1)) // Gmod(2)
# assert GoldenRatio.Phi1 == (GoldenRatio.sqrt5_1 - Gmod(1)) // Gmod(2)
# # assert GoldenRatio.phi1 == (GoldenRatio.sqrt_5_2 + Gmod(1)) // Gmod(2)
# # assert GoldenRatio.phi2 == (GoldenRatio.sqrt_5_1 + Gmod(1)) // Gmod(2)
# assert GoldenRatio.phi2 == (GoldenRatio.sqrt5_2 + Gmod(1)) // Gmod(2)
# assert GoldenRatio.Phi2 == (GoldenRatio.sqrt5_2 - Gmod(1)) // Gmod(2)
